#!/bin/bash

# checks each paraemter for valid input
function check_parameters {
    # make sure we have an instance id 
    if [[ -z "${1}" ]]
    then
        echo "ERROR: Must provide an EC2 Instance ID with the -i parameter"
        return 1
    fi

    # check if the .tf file or directory exists
    if [[ -f "${2}" ]] && [[ "${2}" == *".tf" ]]
    then
        return 0
    else
        if [[ -d "${2}" ]]
        then
            return 0
        else
            echo ""
            echo "***ERROR: File/directory '${2}' does not exist***"
            return 1
        fi
    fi

    return 0
}

# writes out new snapshots, snapshot copies, volumes, and volume attachments for a given ebs volume to ebs_encryption.tf 
function encrypt_volume {

    tmp_id=$(echo ${2} | xargs)
    final_string=$(echo ${tmp_id//-/_})

    # create unencrypted snapshot of the volume
    printf "resource \"aws_ebs_snapshot\" \"snapshot_"${final_string}"\" { \n\tvolume_id = ${2}\n}\n" | tee -a ebs_encryption.tf >/dev/null

    printf "\n" | tee -a ebs_encryption.tf >/dev/null

    # create an encrypted copy of the snapshot
    printf "resource \"aws_ebs_snapshot_copy\" \"snapshot_enc_"${final_string}"\" {\n\tsource_snapshot_id = aws_ebs_snapshot.snapshot_${final_string}.id\n\tsource_region = \"${3}\"\n\tencrypted = true\n} \n" | tee -a ebs_encryption.tf >/dev/null

    printf "\n" | tee -a ebs_encryption.tf >/dev/null

    # create an ebs volume from the encrypted snapshot copy
    printf "resource \"aws_ebs_volume\" \"${final_string}_enc_vol\" {\n\tsnapshot_id = aws_ebs_snapshot_copy.snapshot_enc_${final_string}.id\n\tavailability_zone = ${5}\n\tsize = 40\n} \n" | tee -a ebs_encryption.tf >/dev/null

    printf "\n" | tee -a ebs_encryption.tf >/dev/null

    # attach the new encrypted ebs volume using the same device as the old unencrypted ebs volume
    printf "resource \"aws_volume_attachment\" \"${final_string}_encrypted_att\" {\n\tdevice_name = ${1}\n\tvolume_id = aws_ebs_volume.${final_string}_enc_vol.id\n\tinstance_id = ${4}.id\n} \n" | tee -a ebs_encryption.tf >/dev/null

    printf "\n" | tee -a ebs_encryption.tf >/dev/null

    return 0
}

# comments out a volume attachment resource from the terraform file, if no resource is found, it detaches the volume via aws cli
function detach_volume {
    
    #check get information on the ebs volume from terraform show
    VOLUME_INFO=$(jq '.values.root_module.resources[] | select(.values.id=='\"$(echo ${1} | xargs)\"')' <(echo "${3}"))

    # get the address of the volume
    VOLUME_AD=$(jq '.address' <(echo "${VOLUME_INFO}"))
    
    # detach ebs volume from aws cli if no volume attachment resource is found
    if [[ -z "$VOLUME_AD" ]] || [[ "$5" == "true" ]]
    then
        echo "Detaching ebs volume ${1}....."

        aws ec2 detach-volume --volume-id $(echo ${1} | xargs)

        aws ec2 wait volume-available \
            --volume-ids $(echo ${1} | xargs)
    else

        # get all volume attachment resources from terraform show
        ALL_VOL_ATTS=$(jq '.values.root_module.resources[] | [select(.type=="aws_volume_attachment")]' <(echo "${3}"))

        # get volume attachment resource for the specific ebs volume
        VOL_ATTRIBUTES=$(jq '.[] | select(.values.volume_id=='\"$(echo ${1})\"') | [{name: .name, volume_id: .values.volume_id, instance_id: .values.instance_id}]' <(echo "$ALL_VOL_ATTS"))

        # get the volume attachment name for the ebs volume
        ATT_NAME=$(jq '.[] | select(.instance_id=='"$(echo "${2}")"') | .name' <(echo "${VOL_ATTRIBUTES}"))

        echo "Commenting out \"aws_volume_attachment\" ${ATT_NAME}....."

        export ATT_NAME

        # comment out the old volume attachment resource using perl regular expression
        perl -i -0777 -p -e 's/(resource "aws_volume_attachment" $ENV{ATT_NAME} {(\n|\r|\n\r)^(.*(\n|\r|\n\r))*?})/\/*$&*\//m' ./${4}

        echo "Finished commenting....."
    fi

    return 0
}

# creates snapshots and encrypted snapshot copy for a root volume, can't detach/attach root volume from terraform
function encrypt_root_volume {

    tmp_id=$(echo ${2} | xargs)
    final_string=$(echo ${tmp_id//-/_})

    # create unencrypted snapshot of the volume
    printf "resource \"aws_ebs_snapshot\" \"snapshot_${final_string}\" { \n\tvolume_id = ${2}\n}\n" | tee -a ebs_encryption.tf >/dev/null

    printf "\n" | tee -a ebs_encryption.tf >/dev/null

    # create an encrypted copy of the snapshot
    printf "resource \"aws_ebs_snapshot_copy\" \"snapshot_enc_${final_string}\" {\n\tsource_snapshot_id = aws_ebs_snapshot.snapshot_${final_string}.id\n\tsource_region = \"${3}\"\n\tencrypted = true\n} \n" | tee -a ebs_encryption.tf >/dev/null

    printf "\n" | tee -a ebs_encryption.tf >/dev/null

    # create an ebs volume from the encrypted snapshot copy
    printf "resource \"aws_ebs_volume\" \"${final_string}_enc_vol\" {\n\tsnapshot_id = aws_ebs_snapshot_copy.snapshot_enc_${final_string}.id\n\tavailability_zone = ${5}\n\tsize = 40\n} \n" | tee -a ebs_encryption.tf >/dev/null

    return 0
}

function main {

    function usage {
        echo ""
        echo "Encryption of EBS Volumes attatched to running EC2 Instance"
        echo "Parameters:"
        echo " -i Specify the resource address of the EC2 Instance"
        echo " -p Specify the path to the .tf file containing the aws_volume_attachment resource"
        echo " -h Display help"
        echo ""
        echo " Output:"
        echo "  Terraform file containing snapshot resources and attachment resources in the same directory as other terraform code"
        echo ""
    }

    local INSTANCE_NAME FILE_PATH OPTION
    local OPTIND OPTARG

    INSTANCE_NAME=""
    FILE_PATH=""

    # get the parameters
    while getopts "i:p:h" OPTION
    do  
        case "${OPTION}"
        in  
            i) INSTANCE_NAME="${OPTARG}";;
            p) FILE_PATH="${OPTARG}";;
            h) usage; return 0;;
            \?) echo "Invalid Parameter"; usage; return 1;;
        esac
    done

    # check if any parameters are empty or invalid
    check_parameters "$INSTANCE_NAME" "$FILE_PATH"
    if [[ $? != 0 ]]
    then
        usage
        return 1
    fi

    DIRECTORY="false"
    if [[ -d "$FILE_PATH" ]]
    then
        cd "$FILE_PATH"
        DIRECTORY="true"
    else
        # read the path into an array, change change the last file to just have the directory
        IFS="/" read -a FILE_PATH_ARRAY <<< "${FILE_PATH}"
        FILE=""

        for i in "${!FILE_PATH_ARRAY[@]}"
        do
            if [[ "${FILE_PATH_ARRAY[$i]}" == *".tf" ]]; then
                FILE=${FILE_PATH_ARRAY[$i]}
                FILE_PATH_ARRAY[$i]=""
            fi
        done

        # concatenate back to a string, change to that directory to run terraform commands
        TFSTATE_PATH="$(IFS=/; printf '%s' "${FILE_PATH_ARRAY[*]}")"

        cd "${TFSTATE_PATH}"
    fi

    echo "Running terraform refresh....."

    terraform refresh

    TF_SHOW_INFO=$(terraform show -json)

    echo "Getting information for ${INSTANCE_NAME}....."

    # parse the terraform response to find the info on the ec2 instance 
    EC_INFO=$(jq '.values.root_module.resources[] | select(.address=='\"${INSTANCE_NAME}\"')' <(echo "$TF_SHOW_INFO"))

    if [ ! -z "$EC_INFO" ]
    then   

        echo "Getting information for EBS volumes"

        # get information for all ebs volume attached to the instance
        EBS_INFO=$(jq '.values.ebs_block_device[] | [{device_name: .device_name, volume_id: .volume_id, encrypted: .encrypted}]' <(echo "$EC_INFO")) 
        
        # current instance state
        INSTANCE_STATE=$(jq '.values.instance_state' <(echo "$EC_INFO"))
        
        # instance id
        INSTANCE_ID=$(jq '.values.id' <(echo "$EC_INFO"))
        
        # root ebs volume information for the current instance
        ROOT_INFO=$(jq '.values.root_block_device[] | [{device_name: .device_name, encrypted: .encrypted, volume_id: .volume_id}]' <(echo "$EC_INFO"))

        AVAIL_ZONE=$(jq '.values.availability_zone' <(echo "$EC_INFO"))

        # parse the source region from the availability zone
        tmp=$(echo ${AVAIL_ZONE} | xargs)
        SOURCE_REGION=${tmp%?}

        VOLUMES_ENC="false"

        echo "Starting Encryption....."

        # encrypt all secondary ebs volumes
        for row in $(echo "$EBS_INFO" | jq -c '.[]') 
        do
            _jq() {
                echo ${row} | jq ${1}
            }
            if [[ "$(echo $(_jq '.encrypted'))" == "true" ]]
            then
                echo "Volume $(echo $(_jq '.volume_id')) == encrypted....."
            else
                VOLUMES_ENC="true"
                echo "Volume $(echo $(_jq '.volume_id')) == not encrypted....."

                tmp_device=$(echo $(_jq '.device_name'))
                tmp_id=$(echo $(_jq '.volume_id'))

                encrypt_volume "$tmp_device" "$tmp_id" "$SOURCE_REGION" "$INSTANCE_NAME" "$AVAIL_ZONE"

                detach_volume "$tmp_id" "$INSTANCE_ID" "$TF_SHOW_INFO" $FILE "$DIRECTORY"
            fi
        done
    
        # stop the ec2 instance if encryption is for a root volume
        ENCRYPTED=$(echo "$ROOT_INFO" | jq '.[] | .encrypted')
        if [[ $ENCRYPTED == false ]]
        then
            VOLUMES_ENC="true"

            echo "Root volume == not encrypted....."
            echo "Stopping EC2 Instance ${INSTANCE_NAME}....."
            ROOT=true
            # stop instance if not stopped already
            if [ "${INSTANCE_STATE}" != "stopped" ]
            then 

                aws ec2 stop-instances --instance-ids $(echo $INSTANCE_ID | xargs) 

                echo "Waiting for $INSTANCE_ID to stop....."

                aws ec2 wait instance-stopped \
                --instance-ids $(echo $INSTANCE_ID | xargs) \
                --query 'StoppingInstances[*].[CurrentState.Name, InstanceId]' \
                --output text
            fi

            for row in $(echo "$ROOT_INFO" | jq -c '.[]')
            do 
                _jq() {
                    echo ${row} | jq ${1}
                }

                echo "Root volume == not encrypted....."

                tmp_device=$(echo $(_jq '.device_name'))
                tmp_id=$(echo $(_jq '.volume_id'))

                encrypt_root_volume "$tmp_device" "$tmp_id" "$SOURCE_REGION" "$INSTANCE_NAME" "$AVAIL_ZONE"
                ROOT_REPLACEMENT="true"
                TMP_ID=$tmp_id
                TMP_DEVICE="$tmp_device"
            done
        else
            echo "Root Volume == encrypted....."
        fi 

        if [[ "$VOLUMES_ENC" == "true" ]]
        then
            terraform apply
        else
            echo "All volumes already encrypted.....program exiting....."
            return 0
        fi

        # replace root volume via aws cli if needed
        if [[ "true" == "$ROOT_REPLACEMENT" ]]
        then

            terraform refresh
            
            tmp=$(echo ${TMP_ID} | xargs)
            final_string=$(echo ${TMP_ID//-/_})

            echo $final_string

            echo "Starting root block device replacement....."

            # find the id of the encrypted volume
            INFO=$(terraform show -json)

            VOLUMES_INFO=$(jq '.values.root_module.resources[] | select(.type=="aws_ebs_volume") | [{name: .name, id: .values.id}]' <(echo "$INFO"))

            VOL_COPY=$(jq '.[] | select(.name=='\""${final_string}_enc_vol"\"') | .id' <(echo "$VOLUMES_INFO"))
            tmp2=$(echo ${VOL_COPY} | xargs)
            tmp3=$(echo ${TMP_DEVICE} | xargs)
            tmp4=$(echo $INSTANCE_ID | xargs)

            echo "Detaching old root volume....."

            # detach the old root volume
            aws ec2 detach-volume --volume-id $tmp

            aws ec2 wait volume-available \
                --volume-ids $tmp

            echo "Attaching new root volume....."

            aws ec2 attach-volume --volume-id $tmp2 --instance-id $tmp4 --device $tmp3

            sleep 5

            echo "Restarting EC2 Instance....."

            # restart the instance
            aws ec2 start-instances --instance-ids $tmp4
        fi
            
        echo "Finished encryption for $INSTANCE_NAME....."
    else    
        echo "EC2 INSTANCE $INSTANCE_NAME not found in .tfstate file.....program exiting"
        return 1
    fi


    return 0
}

main "$@"


