# README #

Encryption for ebs volumes attached to running ec2 instance deployed via Terraform. 

### Set Up ###

* User has Terraform installed
* User has AWS CLI installed and has permissions to edit ec2 instances and eb2 volumes. 
* Must have ec2 instance deployed via terraform

### Run ###

* Parameters
    - '-i' -> required
        - AWS instance resource name
    - '-p' -> required
        - Path to the .tf file containing the aws_volume_attachment resource if ebs volumes were deployed via terraform
        - If ebs volumes were not deployed via terraform, give the path to the directory containing the .tf files

* Example Runs
    - ebs volume attachments in .tf file
        - bash EBS-Encryption-Bash.sh -i aws_instance.test_instance -p ../../test/main.tf

    - no ebs volume attachments
        - bash EBS-Encryption-Bash.sh -i aws_instance.test_instance -p ../../test/